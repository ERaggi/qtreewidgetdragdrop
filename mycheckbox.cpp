#include "mycheckbox.h"
#include "maphelpers.h"

#include <QWidgetItem>
#include <QTreeWidget>

MyCheckBoxMap::MyCheckBoxMap(QTreeWidgetItem *mapItem) : QCheckBox(mapItem->treeWidget())
{
    mTreeItem = mapItem;
    setText(mTreeItem->text(MAP_COLUMN));
    mTreeItem->setText(MAP_COLUMN, "");

    connect(this, &MyCheckBoxMap::clicked, this, [&]() {
       setCheckedUnchecked(mTreeItem);
       if(isChecked()) {
           setParentChecked(mTreeItem);
       }
       checkUncheckBasedOnSiblings(this);
    });
}

void MyCheckBoxMap::setCheckedUnchecked(QTreeWidgetItem *myItemMap) const
{
    QWidget *item_widget = Q_NULLPTR;
    QTreeWidget *tree_widget = Q_NULLPTR;
    QTreeWidgetItem *child_item = Q_NULLPTR;
    MyCheckBoxMap *myCheckBoxMap = Q_NULLPTR;
    MyCheckBoxMap *itemCheckBox = Q_NULLPTR;

    tree_widget = myItemMap->treeWidget();
    myCheckBoxMap = static_cast<MyCheckBoxMap*>(tree_widget->itemWidget(myItemMap, MAP_COLUMN));
    const int n_children = myItemMap->childCount();
    for(int a = 0; a < n_children; ++a) {
        child_item = myItemMap->child(a);
        item_widget = tree_widget->itemWidget(child_item, MAP_COLUMN);
        itemCheckBox = static_cast<MyCheckBoxMap*>(item_widget);
        itemCheckBox->setChecked(myCheckBoxMap->isChecked());
        setCheckedUnchecked(child_item);
    }
}

void MyCheckBoxMap::setParentChecked(QTreeWidgetItem *myParentItemMap) const
{
    QWidget *parent_widget = Q_NULLPTR;
    QTreeWidget *tree_widget = Q_NULLPTR;
    QTreeWidgetItem *parent_item = Q_NULLPTR;
    MyCheckBoxMap *parentCheckBoxMap = Q_NULLPTR;

    tree_widget = myParentItemMap->treeWidget();
    parent_item = myParentItemMap->parent();

    if(parent_item) {
        parent_widget = tree_widget->itemWidget(parent_item, MAP_COLUMN);
        parentCheckBoxMap = static_cast<MyCheckBoxMap*>(parent_widget);
        parentCheckBoxMap->setChecked(true);
        setParentChecked(parent_item);
    }
}

void MyCheckBoxMap::checkUncheckBasedOnSiblings(MyCheckBoxMap *MyCBoxMap) const
{
    QWidget *item_widget = Q_NULLPTR;
    QWidget *parent_item_widget = Q_NULLPTR;
    QTreeWidgetItem *item_parent = Q_NULLPTR;
    QTreeWidgetItem *child_item = Q_NULLPTR;
    QTreeWidget *tree_widget = Q_NULLPTR;
    MyCheckBoxMap *myCheckBoxParentMap = Q_NULLPTR;
    MyCheckBoxMap *childCheckBoxMap = Q_NULLPTR;
    int checked_unchecked_count;

    item_parent = MyCBoxMap->mTreeItem->parent();
    if(item_parent) {
        tree_widget = item_parent->treeWidget();
        parent_item_widget = tree_widget->itemWidget(item_parent, MAP_COLUMN);
        myCheckBoxParentMap = static_cast<MyCheckBoxMap*>(parent_item_widget);
        const int n_children = item_parent->childCount();
        checked_unchecked_count = 0;
        for(int a = 0; a < n_children; ++a) {
            child_item = item_parent->child(a);
            item_widget = tree_widget->itemWidget(child_item, MAP_COLUMN);
            childCheckBoxMap = static_cast<MyCheckBoxMap*>(item_widget);
            if(childCheckBoxMap->isChecked() == MyCBoxMap->isChecked()) {
                ++checked_unchecked_count;
            }
        }
        if(checked_unchecked_count == n_children) {
            myCheckBoxParentMap->setChecked(MyCBoxMap->isChecked());
            checkUncheckBasedOnSiblings(myCheckBoxParentMap);
        }
    }
}
