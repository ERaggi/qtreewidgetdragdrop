#ifndef MAPHELPERS_H
#define MAPHELPERS_H

#include <QTreeWidget>
#include <QTreeWidgetItem>

#define MAP_COLUMN 0


enum MapProcessing
{
    SINGLE_CHECKED,
    ALL_CHECKED
};

template<typename MyObject, typename WidgetType, int mode> void qtreewidgetitem_info(QTreeWidget *tree_widget, QTreeWidgetItem *tree_item,
                                                                     MyObject mapProcesor)
{
    QTreeWidgetItem *child_item = Q_NULLPTR;
    QWidget *item_widget = Q_NULLPTR;
    WidgetType *widget = Q_NULLPTR;

    //  here we start to go through the child items of each item (it can be top-level or not)
    if(tree_item->childCount() > 0) {
        const int n_child = tree_item->childCount();
        for(int a = 0; a < n_child; ++a) {
            child_item = tree_item->child(a);
            item_widget = tree_widget->itemWidget(child_item, MAP_COLUMN);
            widget = static_cast<WidgetType*>(item_widget);
            if(mode == ALL_CHECKED) {
                mapProcesor->process(tree_widget, child_item);

                // here we recursively go through the children of the item
                qtreewidgetitem_info<MyObject, WidgetType, mode>(tree_widget, child_item, mapProcesor);
            } else if(mode == SINGLE_CHECKED) {
                if(widget) {
                    if(widget->isChecked()) {
                        mapProcesor->process(tree_widget, child_item);

                        // here we recursively go through the children of the item
                        qtreewidgetitem_info<MyObject, WidgetType, mode>(tree_widget, child_item, mapProcesor);
                    }
                }
            }
        }
    } else {
        // here it has no children, but we can process it directly
        item_widget = tree_widget->itemWidget(tree_item, MAP_COLUMN);
        widget = static_cast<WidgetType*>(item_widget);
        if(mode == ALL_CHECKED) {
            mapProcesor->process(tree_widget, tree_item);
        } else if(mode == SINGLE_CHECKED) {
            if(widget) {
                if(widget->isChecked()) {
                    mapProcesor->process(tree_widget, tree_item);
                }
            }
        }
    }
}

template<typename MyObject, typename WidgetType, int mode> void qtreewidget_info(QTreeWidget *tree_widget,MyObject mapProcesor)
{
    QTreeWidgetItem *top_item = Q_NULLPTR;

    // here we go through the top-level elements to go through their children
    const int n_tops = tree_widget->topLevelItemCount();
    for(int a = 0; a < n_tops; ++a) {
        top_item = tree_widget->topLevelItem(a);

        // here we go through the children of the item top-level
        qtreewidgetitem_info<MyObject, WidgetType, mode>(tree_widget, top_item, mapProcesor);
    }
}

void qtreewidgetitem_assign_qcheckbox(QTreeWidget *tree_widget, QTreeWidgetItem *tree_item);


#endif // MAPHELPERS_H
