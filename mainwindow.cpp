#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "maphelpers.h"
#include "mycheckbox.h"

#include <QTreeWidgetItem>
#include <QStringList>
#include <QTreeWidgetItemIterator>
#include <QDropEvent>
#include <QEvent>
#include <QDragEnterEvent>
#include <QDragLeaveEvent>
#include <QDragMoveEvent>


#include <iostream>
#include <iterator>
#include <vector>

using namespace std;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // FIRST WAY TO CREATE THE TREEE (AND LONGEST WAY)

#if 0

    QTreeWidgetItem *top1 = new QTreeWidgetItem({ "Images" });
    QTreeWidgetItem *top2 = new QTreeWidgetItem({ "Path" });
    QTreeWidgetItem *top3 = new QTreeWidgetItem({ "Segmentation" });

    QList<QTreeWidgetItem*> children1;
    QList<QTreeWidgetItem*> children2;
    QList<QTreeWidgetItem*> children3;

    children1.append(new QTreeWidgetItem({ "Original" }));
    children1.append(new QTreeWidgetItem({ "Sample" }));
    children1.append(new QTreeWidgetItem({ "Black/White" }));

    children2.append(new QTreeWidgetItem({ "Left Side" }));
    children2.append(new QTreeWidgetItem({ "Right Side" }));
    children2.append(new QTreeWidgetItem({ "Center Side" }));

    children3.append(new QTreeWidgetItem({ "Edge Detection" }));
    children3.append(new QTreeWidgetItem({ "Clustering" }));
    children3.append(new QTreeWidgetItem({ "Region-Based" }));
    children3.append(new QTreeWidgetItem({ "Mask RNN" }));


    top1->addChildren(children1);
    top1->setIcon(0, QIcon("/home/labrat/ultrasound_mapper/src/ERDatabaseSystem/ui/qrc/laserscan.png"));
    top1->child(0)->setIcon(0, QIcon("/home/labrat/ultrasound_mapper/src/ERDatabaseSystem/ui/qrc/laserscan.png"));
    top1->child(1)->setIcon(0, QIcon("/home/labrat/ultrasound_mapper/src/ERDatabaseSystem/ui/qrc/laserscan.png"));
    top1->child(2)->setIcon(0, QIcon("/home/labrat/ultrasound_mapper/src/ERDatabaseSystem/ui/qrc/laserscan.png"));

    top2->addChildren(children2);
    top2->setIcon(0, QIcon("/home/labrat/ultrasound_mapper/src/ERDatabaseSystem/ui/qrc/laserscan.png"));
    top2->child(0)->setIcon(0, QIcon("/home/labrat/ultrasound_mapper/src/ERDatabaseSystem/ui/qrc/laserscan.png"));
    top2->child(1)->setIcon(0, QIcon("/home/labrat/ultrasound_mapper/src/ERDatabaseSystem/ui/qrc/laserscan.png"));
    top2->child(2)->setIcon(0, QIcon("/home/labrat/ultrasound_mapper/src/ERDatabaseSystem/ui/qrc/laserscan.png"));

    top3->addChildren(children3);
    top3->setIcon(0, QIcon("/home/labrat/ultrasound_mapper/src/ERDatabaseSystem/ui/qrc/laserscan.png"));
    top3->child(0)->setIcon(0, QIcon("/home/labrat/ultrasound_mapper/src/ERDatabaseSystem/ui/qrc/laserscan.png"));
    top3->child(1)->setIcon(0, QIcon("/home/labrat/ultrasound_mapper/src/ERDatabaseSystem/ui/qrc/laserscan.png"));
    top3->child(2)->setIcon(0, QIcon("/home/labrat/ultrasound_mapper/src/ERDatabaseSystem/ui/qrc/laserscan.png"));
    top3->child(3)->setIcon(0, QIcon("/home/labrat/ultrasound_mapper/src/ERDatabaseSystem/ui/qrc/laserscan.png"));



#endif

    // SECOND WAY TO CREATE THE TREEE (SHORTEST AND MAINTAINABLE WAY)

    ui->treeWidget->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->treeWidget->setDragEnabled(true);
    ui->treeWidget->viewport()->setAcceptDrops(true);
    ui->treeWidget->setDropIndicatorShown(true);
    ui->treeWidget->setDragDropMode(QAbstractItemView::InternalMove);

    auto *top1 = createItem("Images", "/home/labrat/ultrasound_mapper/src/ERDatabaseSystem/ui/qrc/laserscan.png");
    auto *top2 = createItem("Path", "/home/labrat/ultrasound_mapper/src/ERDatabaseSystem/ui/qrc/laserscan.png");

    top1->addChild(createItem("Original", "/home/labrat/ultrasound_mapper/src/ERDatabaseSystem/ui/qrc/laserscan.png"));
    top1->addChild(createItem("Sample", "/home/labrat/ultrasound_mapper/src/ERDatabaseSystem/ui/qrc/laserscan.png"));

    top2->addChild(createItem("Left Side", "/home/labrat/ultrasound_mapper/src/ERDatabaseSystem/ui/qrc/laserscan.png"));
    top2->addChild(createItem("Right Side", "/home/labrat/ultrasound_mapper/src/ERDatabaseSystem/ui/qrc/laserscan.png"));

    ui->treeWidget->addTopLevelItems({ top1, top2 });

    const int n_tops = ui->treeWidget->topLevelItemCount();

    for(int a = 0; a<n_tops; ++a) {
        const int n_children = ui->treeWidget->topLevelItem(a)->childCount();
        qtreewidgetitem_assign_qcheckbox(ui->treeWidget, ui->treeWidget->topLevelItem(a));
        for(int b = 0; b<n_children; ++b) {
            qtreewidgetitem_assign_qcheckbox(ui->treeWidget, ui->treeWidget->topLevelItem(a)->child(b));
        }
    }

//    QTreeWidgetItem *parentItem = Q_NULLPTR;
//    QTreeWidgetItem *childItem = Q_NULLPTR;


//    QTreeWidgetItem* parentItem = new QTreeWidgetItem();
//    parentItem->setText(0, "Test");
//    parentItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsDropEnabled);

//    for(int i = 0; i < 10; ++i)
//    {
//      QTreeWidgetItem* pItem = new QTreeWidgetItem(parentItem);
//      pItem->setText(0, QString("Number %1").arg(i) );
//      pItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsDragEnabled);
//      pItem->addChild(pItem);
//    }


//    QList<QTreeWidgetItem*>::const_iterator iter_parent;
//    QList<QTreeWidgetItem*>::const_iterator iter_children;

//    QTreeWidgetItemIterator it(ui->treeWidget);
//       while (*it) {
//           parentItem = new QTreeWidgetItem(ui->treeWidget);
//           //parentItem->setText(0, it.key());
//           foreach (const auto &str, it) {
//               childItem = new QTreeWidgetItem;
//               childItem->setText(0, str);
//               parentItem->addChild(childItem);
//           }
//       }

//    QMapIterator<QString, QStringList> i(treeMap);
//    while (i.hasNext()) {
//        i.next();
//        parentItem = new QTreeWidgetItem(widget);
//        parentItem->setText(0, i.key());
//        foreach (const auto &str, i.value()) {
//            childItem = new QTreeWidgetItem;
//            childItem->setText(0, str);
//            parentItem->addChild(childItem);
//        }
//    }


}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::process(QTreeWidget *tree_widget, QTreeWidgetItem *tree_item)
{
    MyCheckBoxMap *checkbox = static_cast<MyCheckBoxMap*>(tree_widget->itemWidget(tree_item, MAP_COLUMN));
    if(!listwidget_exists_item(checkbox->text())) {
        ui->listWidget->addItem(checkbox->text());
    }
}


void MainWindow::on_pushButton_clicked()
{
    ui->listWidget->clear();
    qtreewidget_info<MainWindow*, MyCheckBoxMap, SINGLE_CHECKED>(ui->treeWidget, this);
}

bool MainWindow::listwidget_exists_item(const QString &item) const
{
    const int n_items = ui->listWidget->count();
    for(int a = 0; a < n_items; ++a) {
        if(ui->listWidget->item(a)->text() == item) {
            return true;
        }
    }
    return false;
}


QTreeWidgetItem *MainWindow::createItem(const QString &name, const QString &iconPath)
{
    auto *item = new QTreeWidgetItem(QStringList{name});
    item->setIcon(0, QIcon(iconPath));
    return item;
}








