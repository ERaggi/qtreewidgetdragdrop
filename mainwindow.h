#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class QTreeWidget;
class QTreeWidgetItem;


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    virtual void process(QTreeWidget *tree_widget, QTreeWidgetItem *tree_item);

    QTreeWidgetItem *createItem(const QString &name, const QString &iconPath);

    QString name() const {
        return m_Name;
    }

    QString iconPath() const {
        return m_Path;
    }

    void setName(const QString &name)
    {
        m_Name = name;
    }

    void setPath(const QString &path)
    {
        m_Path = path;
    }

private slots:
    void on_pushButton_clicked();

private:
    Ui::MainWindow *ui;
    QTreeWidget *widget;

    bool listwidget_exists_item(const QString &item) const;

    QString m_Name;
    QString m_Path;

    };
#endif // MAINWINDOW_H
