#ifndef MYCHECKBOX_H
#define MYCHECKBOX_H

#include <QCheckBox>
#include <QTreeWidgetItem>


class MyCheckBoxMap : public QCheckBox
{
    Q_OBJECT

public:
    MyCheckBoxMap(QTreeWidgetItem *mapItem);

    QTreeWidgetItem *treeItem() const {
        return mTreeItem;
    }

private:
    QTreeWidgetItem *mTreeItem;

    void setCheckedUnchecked(QTreeWidgetItem *myItemMap) const;
    void setParentChecked(QTreeWidgetItem *myParentItemMap) const;
    void checkUncheckBasedOnSiblings(MyCheckBoxMap *MyCBoxMap) const;
};

#endif // MYCHECKBOX_H
